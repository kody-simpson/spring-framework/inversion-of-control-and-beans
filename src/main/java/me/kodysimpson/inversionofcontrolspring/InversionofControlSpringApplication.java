package me.kodysimpson.inversionofcontrolspring;

import me.kodysimpson.inversionofcontrolspring.services.KitchenService;
import me.kodysimpson.inversionofcontrolspring.services.RestaurantService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class InversionofControlSpringApplication {

    public static void main(String[] args) {

        //Get a ref to the ApplicationContext
        ApplicationContext applicationContext = SpringApplication.run(InversionofControlSpringApplication.class, args);

        //How to access a Bean from the Application Context
        KitchenService kitchenService = (KitchenService) applicationContext.getBean("kitchenService");
        //or do this:
        KitchenService kitchenService1 = applicationContext.getBean(KitchenService.class);

        //do manual constructor depedency injection
        RestaurantService restaurantService = new RestaurantService(kitchenService);
        restaurantService.processOrder();


        //Even the Restaurant Service can be a Bean and the KitchenService will be automatically injected into it
//        RestaurantService restaurantService = applicationContext.getBean(RestaurantService.class);
//        restaurantService.processOrder();

    }

}
