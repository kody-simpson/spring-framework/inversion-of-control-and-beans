package me.kodysimpson.inversionofcontrolspring.services;

import org.springframework.stereotype.Service;

@Service //class level annotation or Spring Stereotype that marks this class as a Spring Bean
//@Component does the same thing, top level bean annotation
//Can also specify the bean name by providing a value to the annotation
public class KitchenService {

    public void cookFood(){
        System.out.println("Sss... *food cooking*");
    }

}
