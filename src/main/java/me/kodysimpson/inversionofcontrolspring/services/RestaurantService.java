package me.kodysimpson.inversionofcontrolspring.services;

import org.springframework.stereotype.Service;

@Service //this client class can be a bean too!
public class RestaurantService {

    //this is our depedency
    private KitchenService kitchenService;

    //ask for the dependency to be passed in
    public RestaurantService(KitchenService kitchenService){
        this.kitchenService = kitchenService;
    }

    public void processOrder(){
        System.out.println("New order started.");
        kitchenService.cookFood();
    }

}
